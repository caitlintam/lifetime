<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lifeti24_wp931');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'grlvlsttkgl3klwt55g5fcei5jgm1zcrng3bsra6qs56rnk4r8ihp8qhwpslbkpz');
define('SECURE_AUTH_KEY',  'p2qws9plbpjssqjuonwihpxidgxrpxm0q7xj0oksw3vydfqyacpdf1ii5wbxgzq3');
define('LOGGED_IN_KEY',    'd1fxtzjyjcuvp8gxigklcjz3qm3vlkw0kmihde5vtkajxx65b8swznaspfexnxdc');
define('NONCE_KEY',        'rfn0v69epucu4lkwfimgss4nyn42iirx5gui1zfnyqfar5vpv5prtn1exwkdflfb');
define('AUTH_SALT',        'plp0ymbi3gh61bhhrvsj517z6qfzypedosq26hincqfjf6wgrmylktrqgmjo8foo');
define('SECURE_AUTH_SALT', 'ebp56w5qyqtrk5g3apj62ip3xswzseai8izhynytdgocoyricyxosalzbprlc9ez');
define('LOGGED_IN_SALT',   'inkcxszxecgukvb6tjxx7ryy1rsrw6msoz3ykfkuvky2kpkfqaosd4vrb19t7l99');
define('NONCE_SALT',       'vqvn8z9grbod9tfe9fnusleyjkp7s7u4dl6actwjqu9aji6l7d5bttlxnlkgwhqi');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp9h_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
